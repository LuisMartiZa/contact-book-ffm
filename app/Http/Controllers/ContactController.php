<?php
/**
 * Created by PhpStorm.
 * User: luismartinezzarza
 * Date: 12/11/16
 * Time: 12:50
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Group;
use \Auth;

class ContactController extends Controller
{

    /**
     * Display a listing of the Contacts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('search')) {
            $search = $request->get('search');
            $contacts = $this->searchContact($search);

        } else {
            $contacts = Contact::where('user_id','=', Auth::user()->id)->paginate(5);
        }

        return view('contacts.index',compact('contacts'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new Contact.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = Group::lists('name', 'id');

        return view('contacts.create', compact('groups'));
    }

    /**
     * Store new Contact in DB.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|unique:contacts',
            'address' => 'required'
        ]);

        $contactParams = $request->all();
        $contactParams['user_id'] = Auth::user()->id;
        $groups = null;

        if(isset($contactParams['groups'])) {
            $groups = $contactParams['groups'];
            unset($contactParams['groups']);
        }

        $contact = Contact::create($contactParams);

        if(!is_null($groups)) {
            foreach ($groups as $groupId) {
                $contact->groups()->attach($groupId);
            }
        }

        return redirect()->route('contacts.index')
            ->with('success','Contact was created successfully');
    }

    /**
     * Display the specified Contact.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::find($id);
        //$groups = $contact->groups();
        return view('contacts.show',compact('contact'));
    }

    /**
     * Show the form for editing selected contact.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::find($id);
        $contactGroups = $contact->groups()->lists('id')->toArray();
        $groups = Group::lists('name', 'id');

        return view('contacts.edit',compact('contact','groups','contactGroups'));
    }

    /**
     * Update contact in DB.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|unique:contacts',
            'address' => 'required'
        ]);

        $contact = Contact::find($id);
        $groups = null;

        $contactParams = $request->all();

        if(isset($contactParams['groups'])) {
            $groups = $contactParams['groups'];
            unset($contactParams['groups']);
        }

        $contact->update($contactParams);

        if(!is_null($groups)) {
            $groupsIDs = array();
            foreach ($groups as $groupId) {
                $groupsIDs[] = $groupId;
            }
            $contact->groups()->sync($groupsIDs);
        } else {
            $contact->groups()->detach();
        }

        return redirect()->route('contacts.index')
            ->with('success','Contact was updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->groups()->detach();
        $contact->delete($id);

        return redirect()->route('contacts.index')
            ->with('success','Contact was removed successfully');
    }

    private function searchContact($queryValue)
    {
        $contacts = Contact::whereHas('groups', function ($query) use($queryValue) {
            $query->where('name', 'like', $queryValue);
        })->paginate(5);

        if($contacts->isEmpty()) {
            $contacts = Contact::where('user_id','=', Auth::user()->id)
                ->Where('name','like',$queryValue)
                ->orWhere('surname','like',$queryValue)
                ->orWhere('email','like',$queryValue)
                ->orWhere('address','like',$queryValue)
                ->orWhere('phone','like',$queryValue)
                ->paginate(5);
        }

        return $contacts;
    }
}