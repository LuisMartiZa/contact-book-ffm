<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();
Route::get('/logout', function () {
    Auth::logout();
    return redirect('/');
});
//Route::get('contacts/search', 'ContactController@search');
Route::resource('contacts','ContactController');
Route::resource('groups','GroupController');


Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@dashboard');

