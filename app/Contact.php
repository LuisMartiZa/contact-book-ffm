<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    public $fillable = ['name','surname','email','address','phone','user_id'];

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group','contact_groups','contact_id','group_id');
    }
}
