@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <h1 class="panel-title pull-left" style="padding-top: 15px;">CONTACTS</h1>
                        {!! Form::open(['method' => 'GET','route' => ['contacts.index'],'class'=>'form navbar-form navbar-right searchform']) !!}
                        {!! Form::text('search', null, array('class'=>'form-control',
                                                                'placeholder'=>'Search for a contact...')) !!}
                        {!! Form::submit('Search', array('class'=>'btn btn-default')) !!}
                        {!! Form::close() !!}
                        <div class="btn-group pull-right">
                            <a class="btn btn-success" style="top: 8.5px;" href="{{ route('contacts.create') }}"> Create New Contact</a>
                        </div>
                    </div>

                    <div class="panel-body">

                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        @if(sizeof($contacts) != 0)
                            <table class="table table-bordered">
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Surname</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th width="280px">Actions</th>
                                </tr>
                                @foreach ($contacts as $contact)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $contact->name }}</td>
                                        <td>{{ $contact->surname }}</td>
                                        <td>{{ $contact->email }}</td>
                                        <td>{{ $contact->address }}</td>
                                        <td>{{ $contact->phone }}</td>
                                        <td>
                                            <a class="btn btn-info" href="{{ route('contacts.show',$contact->id) }}">Show</a>
                                            <a class="btn btn-primary" href="{{ route('contacts.edit',$contact->id) }}">Edit</a>
                                            {!! Form::open(['method' => 'DELETE','route' => ['contacts.destroy', $contact->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                            {!! $contacts->render() !!}
                        @else
                             <div>There is not Contacts.</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
