@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <ul class="list-group">
                        <a class="list-group-item" href="{{ url('/contacts') }}">Contacts</a>
                        <a class="list-group-item" href="{{ url('/groups') }}">Contacts groups</a>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
