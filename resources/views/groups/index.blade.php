@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <h1 class="panel-title pull-left" style="padding-top: 7.5px;">GROUPS</h1>
                        <div class="btn-group pull-right">
                            <a class="btn btn-success" href="{{ route('groups.create') }}"> Create New Group</a>
                        </div>
                    </div>

                    <div class="panel-body">

                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                        @if(sizeof($groups) != 0)
                            <table class="table table-bordered">
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th width="280px">Actions</th>
                                </tr>
                                @foreach ($groups as $group)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $group->name }}</td>
                                        <td>
                                            <a class="btn btn-info" href="{{ route('groups.show',$group->id) }}">Show</a>
                                            <a class="btn btn-primary" href="{{ route('groups.edit',$group->id) }}">Edit</a>
                                            {!! Form::open(['method' => 'DELETE','route' => ['groups.destroy', $group->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                            {!! $groups->render() !!}
                        @else
                             <div>There is not groups.</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
