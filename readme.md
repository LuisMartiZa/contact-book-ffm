# Famous Four Media Technical Challenge

Hello, it's a pleasure that you are reading this readme.  Here is the technical challenge called Contact Book.
## Deployment instructions

If you want to use this application, you must clone this repository in your environment or use
a Vagrant machine. 

If you choose the second, you must follow the next tutorial: [Laravel homestead](https://laravel.com/docs/5.2/homestead).
When you have downloaded the machine and clone homestead, you must edit ~/.homestead/Homestead.yaml file and set this config:

        ---
        ip: "192.168.10.10"
        memory: 2048
        cpus: 2
        provider: virtualbox
        authorize: ~/.ssh/id_rsa.pub
        
        keys:
            - ~/.ssh/id_rsa
        
        folders:
            - map: ~/code/contact-book-ffm
              to: /home/vagrant/Code
        
        sites:
            - map: contact.book.app
              to: /home/vagrant/Code/public
        
        databases:
            - contactbook
        
        # blackfire:
        #     - id: foo
        #       token: bar
        #       client-id: foo
        #       client-token: bar
        
        # ports:
        #     - send: 50000
        #       to: 5000
        #     - send: 7777
        #       to: 777
        #       protocol: udp

You must create the folder ~/code and clone there the repository. Then you must edit your /etc/hosts file and set the next:

        contact.book.app    192.168.10.10

Finally go to the folder where you clone homestead and run this command:

        vagrant up
        
That's OK. Now we have our environment up and we go to finalize the process.

Now you must enter in your environment or in vagrant machine via ssh. For vagrant: 

        vagrant ssh
        
When you have connected via ssh you must run this command on root folder of project. If you use vagrant you must do this on 
~/Code.

        php artisan migrate
        
With this command we populate the tables in database and now we can use the application. 

## Thank you!

Thank you for visit this repository. If there is any problem contact with me Luis Martínez <luismartiza88@gmail.com>
